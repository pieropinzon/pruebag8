import re
from pyramid.response import Response
from pyramid.view import view_config
from sqlalchemy.exc import DBAPIError
from pyramid.httpexceptions import HTTPFound


from .models import (
    DBSession,
    MyModel,
    Usuario,
    )


@view_config(route_name='home', renderer='templates/mytemplate.pt')
def my_view(request):
    try:
        one = DBSession.query(MyModel).filter(MyModel.name == 'one').first()
    except DBAPIError:
        return Response(conn_err_msg, content_type='text/plain', status_int=500)
    return {'one': one, 'project': 'RetoG8'}


@view_config(route_name='registro', renderer='templates/formulario.pt')
def registro(request):
    response = {}
    response2 = {}
    response['value'] = ''
    response['error'] = None
    response['url'] = '/registro'

    if 'nombre' in request.params:

        #Valida los datos
        nombre = request.params['nombre']
        if not nombre:
            response['error'] = 'True'
        apellido = request.params['apellido']
        if not apellido:
            response['error'] = 'True'
        telefono = request.params['telefono']
        if not telefono:
            response['error'] = 'True'
        correo = request.params['correo']
        match = re.search(r'[\w.-]+@[\w.-]+\.\w', correo)
        if correo and not match:
            response['error'] = 'True'
        direccion = request.params['direccion']
        if not direccion:
            response['error'] = 'True'

        #Fin de las validaciones

        if response['error'] == None:
            usuario = Usuario(nombre=nombre,
                apellido=apellido,
                telefono=telefono,
                correo=correo,
                direccion=direccion,
                )
            DBSession.add(usuario)
            return HTTPFound(location='/listo')

    return response

@view_config(route_name='mientras', renderer='templates/listo.pt')
def my_view(request):
    return {}