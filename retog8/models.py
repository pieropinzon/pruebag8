from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    String,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class MyModel(Base):
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    value = Column(Integer)

class Usuario(Base):
    __tablename__ = u'usuarios'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(25))
    apellido = Column(String(25))
    correo = Column(String(70))
    telefono = Column(String(12))
    direccion = Column(String(180))

